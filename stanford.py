from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.link import TCLink

class standfordTopo(Topo):
	def __init__(self, **kargs):
		# Inicializa a topologia
		Topo.__init__(self, **kargs)

		goza = self.addHost('h1')
		coza = self.addHost('h2')
		boza = self.addHost('h3')
		yoza = self.addHost('h4')
		poza = self.addHost('h5')
		pozb = self.addHost('h6')
		roza = self.addHost('h7')

		s = []
		for i in range(1, 6):
			s.append(self.addSwitch('s' + str(i)))
	
		# goza -> coza #
		self.addLink(goza, s[0])
		self.addLink(s[3], coza)

		# boza -> poza #
		self.addLink(boza, s[4])
		self.addLink(s[1], poza)
	
		# boza -> coza #
		self.addLink(boza, s[4])
		self.addLink(s[3], coza)

		# UDP Traffic #
		self.addLink(poza, s[1])
		self.addLink(s[1], yoza)
		self.addLink(roza, s[2])
		self.addLink(s[2], yoza)

		# O resto das conexoes entre hosts e switchs #
		self.addLink(pozb, s[0])

		# As conexoes entre os switches #
		for i in range(0, 4):
			self.addLink(s[i], s[i+1])

def start():
	topo = standfordTopo()
	net = Mininet(topo=topo, link=TCLink)
	net.start()
	CLI(net)
	net.stop()

if __name__ == "__main__":
	# Descomente para ter mais informacoes sobre a formacao da rede.
	setLogLevel('info')
	start()

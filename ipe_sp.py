from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.link import TCLink

class ipeTopo(Topo):
	def __init__(self, **kargs):
		# Inicializa a topologia
		Topo.__init__(self, **kargs)

		# Rio Grande do Sul #

		ebt_rs = self.addHost('h1')
		ptt_rs = self.addHost('h2')
		mia = self.addHost('h3')

		rs = self.addSwitch('s1')

		self.addLink(ebt_rs, rs, bw=1)
		self.addLink(ptt_rs, rs, bw=10)
		self.addLink(mia, rs, bw=3)

		#####################

		# Santa Catarina #

		ptt_sc = self.addHost('h4')

		sc = self.addSwitch('s2')
		
		self.addLink(ptt_sc, sc, bw=10)
		self.addLink(sc, rs, bw=10)
		
		#####################

		# Parana #
	
		ptt_pr = self.addHost('h5')
	
		pr = self.addSwitch('s3')
	
		self.addLink(ptt_pr, pr, bw=10)
		self.addLink(pr, rs, bw=10)

		#####################

		# Sao Paulo #

		ansp = self.addHost('h6')
		ptt_tr = self.addHost('h7')
		clara = self.addHost('h8')
		ebt_sp = self.addHost('h9')
		ptt_sp = self.addHost('h10')
		
		sp = self.addSwitch('s4')
	
		self.addLink(ansp, sp)
		self.addLink(ptt_tr, sp)
		self.addLink(clara, sp)
		self.addLink(ebt_sp, sp)
		self.addLink(ptt_sp, sp)
		self.addLink(sp, sc)
		self.addLink(sp, pr)

def start():
	topo = ipeTopo()
	net = Mininet(topo=topo, link=TCLink)
	net.start()
	CLI(net)
	net.stop()

if __name__ == "__main__":
	setLogLevel('info')
	start()

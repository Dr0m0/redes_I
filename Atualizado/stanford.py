from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.link import TCLink

class Stanford:
	def __init__(self, net):
		net.addController('c0')

		self.goza = net.addHost('h1')
		self.coza = net.addHost('h2')
		self.boza1 = net.addHost('h3')
		self.boza2 = net.addHost('h4')
		self.yoza = net.addHost('h5')
		self.poza = net.addHost('h6')
		self.pozb = net.addHost('h7')
		self.roza = net.addHost('h8')

		self.s = []
		for i in range(1, 6):
			self.s.append(net.addSwitch('s' + str(i)))
		self.bbra = net.addSwitch('s6')
	
		# Conexoes de s1
		self.s1_goza = net.addLink(self.goza, self.s[0], bw=30)
		self.s1_pozb = net.addLink(self.pozb, self.s[0], bw=30)

		# Conexoes de s2
		self.s2_poza = net.addLink(self.poza, self.s[1], bw=30)

		# Conexoes de s3
		self.s3_roza = net.addLink(self.roza, self.s[2], bw=30)

		# Conexoes de s4
		self.s3_coza = net.addLink(self.coza, self.s[3], bw=30)
	
		# Conexoes de s5
		self.s5_boza1 = net.addLink(self.boza1, self.s[4], bw=30)
		self.s5_boza2 = net.addLink(self.boza2, self.s[4], bw=30)

		# Conexoes de bbra
		self.bbra_s1 = net.addLink(self.s[0], self.bbra, bw=30)
		self.bbra_s2 = net.addLink(self.s[1], self.bbra, bw=30)
		self.bbra_yoza = net.addLink(self.yoza, self.bbra, bw=30)
		self.bbra_s2 = net.addLink(self.s[2], self.bbra, bw=30)
		self.bbra_s3 = net.addLink(self.s[3], self.bbra, bw=30)
		self.bbra_s4 = net.addLink(self.s[4], self.bbra, bw=30)

def forwarding_error(stf):
	# Insere o erro #
	stf.s5_boza1.intf1.config(loss=100)

	# Realiza os testes #
	print stf.goza.cmd('ping -c1', stf.coza.IP())
	print stf.boza1.cmd('ping -c1', stf.coza.IP())
	print stf.boza2.cmd('ping -c1', stf.poza.IP())

def congestion(net, stf):
	stf.poza.cmd('ping ' + stf.yoza.IP() + " > test.out &")
	stf.yoza.cmd('sleep 4')
	net.iperf(
		hosts=(stf.poza, stf.yoza), 
		l4Type='UDP', 
		udpBw='100M', 
		seconds=1)
	print 'O resultado do teste foi armazenado no arquivo test.out'

def available_bw(net, stf, base_bw_usage, max_bw):
	print stf.poza.cmd(
		'sleep 0.5; ping ' +
		' -c' + str(max_bw) + 
		' -i 1 ' + stf.yoza.IP() +
		" > test.out &")

	for i in range(1, max_bw+1):
		net.iperf(
			hosts=(stf.poza, stf.yoza),
			l4Type='UDP',
			udpBw=str(2*i + base_bw_usage) + 'M',
			seconds=1)

def priority(net, stf, bw_a, bw_b, sample_size):
	stf.goza.cmd(
		'sleep 0.5; ' +
		'ping -i 1 ' +
		'-c' + str(sample_size) + 
		' ' + stf.coza.IP() + 
		' > a.out &')
	stf.boza1.cmd(
		'sleep 0.5; ' +
		'ping -i 1 ' +
		'-c' + str(sample_size) + 
		' ' + stf.poza.IP() + 
		' > b.out &')

	for i in range(sample_size):
		net.iperf(
			hosts=(stf.goza, stf.coza),
			l4Type='UDP',
			udpBw=str(bw_a) + 'M',
			seconds=1,
			port=10000)
		net.iperf(
			hosts=(stf.boza1, stf.poza),
			l4Type='UDP',
			udpBw=str(bw_b) + 'M',
			seconds=1,
			port=20000)

def start():
	net = Mininet(autoStaticArp=True, link=TCLink)
	stf = Stanford(net)
	net.start()
	#forwarding_error(stf)
	#congestion(net, stf)
	#available_bw(net, stf, 10, 40)
	priority(net, stf, 1, 40, 10)
	CLI(net)
	net.stop()

if __name__ == "__main__":
	# Descomente para ter mais informacoes sobre a formacao da rede.
	setLogLevel('info')
	start()

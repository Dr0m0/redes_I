from mininet.cli import CLI
from mininet.log import setLogLevel
from mininet.net import Mininet
from mininet.topo import Topo
from mininet.link import TCLink

class StanfordTopo(Topo):
	def __init__(self, **kargs):
		Topo.__init__(self, **kargs)

		goza = self.addHost('h1')
		coza = self.addHost('h2')
		boza1 = self.addHost('h3')
		boza2 = self.addHost('h4')
		yoza = self.addHost('h5')
		poza = self.addHost('h6')
		pozb = self.addHost('h7')
		roza = self.addHost('h8')

		s = []
		for i in range(1, 6):
			s.append(self.addSwitch('s' + str(i)))
		bbra = self.addSwitch('s6')
	
		# Conexoes de s1
		s1_goza = self.addLink(goza, s[0])
		s1_pozb = self.addLink(pozb, s[0])

		# Conexoes de s2
		s2_poza = self.addLink(poza, s[1])

		# Conexoes de s3
		s3_roza = self.addLink(roza, s[2])

		# Conexoes de s4
		s3_coza = self.addLink(coza, s[3])
	
		# Conexoes de s5
		s5_boza1 = self.addLink(boza1, s[4], loss=100)
		s5_boza2 = self.addLink(boza2, s[4])

		# Conexoes de bbra
		bbra_s1 = self.addLink(s[0], bbra)
		bbra_s2 = self.addLink(s[1], bbra)
		bbra_yoza = self.addLink(yoza, bbra)
		bbra_s2 = self.addLink(s[2], bbra)
		bbra_s3 = self.addLink(s[3], bbra)
		bbra_s4 = self.addLink(s[4], bbra)

		#s5_boza1.intf1.config(loss=100)
		#print goza.cmd('ping -c1', coza)
		#print boza1.cmd('ping -c1', coza)
		#print boza2.cmd('ping -c1', poza)

def start():
	net = Mininet(topo=StanfordTopo(), link=TCLink)
	net.start()
	CLI(net)
	net.stop()

if __name__ == "__main__":
	# Descomente para ter mais informacoes sobre a formacao da rede.
	setLogLevel('info')
	start()

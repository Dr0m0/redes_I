# Cria a lista de prioridades
tc class del dev eth0 parent 100: classid 100:1 htb rate 10mbps
tc qdisc del dev eth0 root handle 100: htb 

# Define os pacotes que entraram na lista
tc filter del dev eth0 protocol ip parent 100: prio 1 u32 \
match ip dport 10000 0xffff flowid 100:1
